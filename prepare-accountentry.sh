#!/bin/sh
#
# Use this script to create the payments-accountentry topic
#

docker-compose exec broker \
  kafka-topics --create --topic payments-accountentry \
  --partitions 12 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181

docker-compose exec broker \
  kafka-topics --create --topic payments-accountentry-flattened \
  --partitions 12 --replication-factor 1 \
  --if-not-exists --zookeeper zookeeper:2181

