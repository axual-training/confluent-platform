#!/bin/bash
#
# Use this sript to start a producer on the training topic
#

docker-compose exec broker \
  kafka-console-producer --request-required-acks 1 \
	--broker-list broker:9092 --topic training
