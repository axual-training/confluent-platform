#!/bin/bash
#
# Use this script to read the events on the payments-accountentry topic using the command line consumer
#

docker-compose exec schema_registry \
  kafka-avro-console-consumer --bootstrap-server broker:9092 \
   --topic payments-accountentry  --property schema.registry.url=http://schema_registry:8081
