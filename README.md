# Confluent platform project
Use this docker-compose file if you want to quickly set up a single node Kafka cluster _including_ the Confluent Schema Registry and Zookeeper.

### Usage

#### Starting the cluster daemonized (omit the -d when you want to start in the foreground)
```
docker-compose up -d
```

Before you continue, first perform some checks:

Check whether Zookeeper has successfully started:
```
docker-compose logs zookeeper | grep -i binding
```

Check whether the Kafka broker has successfully started: 
```
docker-compose logs broker | grep -i started
```

#### Stopping the cluster and removing everything
```
docker-compose down -v
```

### Source
https://docs.confluent.io/current/installation/docker/docs/quickstart.html#getting-started-with-docker-client
